#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Layer Combinations - Export all object combinations for 2 layers - An Inkscape 1.1+ extension
##############################################################################


import inkex

from copy import deepcopy
import os, uuid, sys


def solo_object(self, layer, visible_object):
    children = layer.getchildren()
    for child in children:
        child.style['display'] = 'none'

    visible_object.style.pop('display')


def export_svg(self, svg):
    doc = svg.tostring().decode('utf-8')

    output_folder = self.options.output_folder

    output_filename = str(uuid.uuid4()) + '.svg'
    output_filepath = os.path.join(output_folder, output_filename)

    with open(output_filepath, 'w') as debug_file:
        debug_file.write(doc)


class LayerCombinations(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--output_folder", type=str, dest="output_folder", default=None)

    def effect(self):

        svg_copy = deepcopy(self.svg)

        layers = svg_copy.xpath('//svg:g[@inkscape:groupmode="layer"]')

        if len(layers) < 2:
            inkex.errormsg('Only one Layer found !')
            return
        if len(layers) > 2:
            inkex.errormsg('More than two layers found !')
            return

        layer_0 = layers[0]
        layer_1 = layers[1]

        for layer_0_child in layer_0.getchildren():
            solo_object(self, layer_0, layer_0_child)
            for layer_1_child in layer_1.getchildren():
                solo_object(self, layer_1, layer_1_child)

                try:
                    export_svg(self, svg_copy)
                except:
                    inkex.errormsg('Cannnot write to folder')
                    sys.exit()


if __name__ == '__main__':
    LayerCombinations().run()
